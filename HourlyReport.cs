﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wilson
{
    public partial class HourlyReport : Form
    {
        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        DataTable dataTable = new DataTable();
        public HourlyReport()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 返回按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label22_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// 下載報表按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label1_Click(object sender, EventArgs e)
        {
            var lines = new List<string>();

            string[] columnNames = dataTable.Columns
                .Cast<DataColumn>()
                .Select(column => column.ColumnName)
                .ToArray();

            var header = string.Join(",", columnNames.Select(name => $"\"{name}\""));
            lines.Add(header);

            var valueLines = dataTable.AsEnumerable()
                .Select(row => string.Join(",", row.ItemArray.Select(val => $"\"{val}\"")));

            lines.AddRange(valueLines);

            File.WriteAllLines("HourlyReport.csv", lines, System.Text.Encoding.UTF8);

            CopyToUSB();


        }

        private void HourlyReport_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            int x = (System.Windows.Forms.SystemInformation.WorkingArea.Width - this.Size.Width) / 2;
            int y = (System.Windows.Forms.SystemInformation.WorkingArea.Height - this.Size.Height) / 2;
            this.StartPosition = FormStartPosition.Manual; //窗體的位置由Location屬性決定
            this.Location = (Point)new Size(x, y);         //窗體的起始位置為(x,y)
            Common comm = new Common();
            dataTable = comm.QueryHourlyReport(Record.global_RecordMachineNum);
            dataGridView1.DataSource = dataTable;
            this.dataGridView1.DefaultCellStyle = new DataGridViewCellStyle
            {
                ForeColor = ColorTranslator.FromHtml("#8F7F6F"),
                BackColor = ColorTranslator.FromHtml("#394056"),
                Font = new Font("Tahoma", 13, FontStyle.Bold),
                SelectionForeColor = ColorTranslator.FromHtml("#8F7F6F"),
                SelectionBackColor = ColorTranslator.FromHtml("#394056")
            };
            this.dataGridView1.GridColor = ColorTranslator.FromHtml("#708090");

            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#394056");
            this.dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 13, FontStyle.Bold);
            this.dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = ColorTranslator.FromHtml("#E3F6FF");

            // Set your desired AutoSize Mode:
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }
        private void CopyToUSB()
        {
            string sourceDir = System.Environment.CurrentDirectory;
            string backupDir = config.AppSettings.Settings["usb_dir_location"].Value;

            try
            {
                string[] txtList = Directory.GetFiles(sourceDir, "HourlyReport.csv");

                // Copy text files. 
                foreach (string f in txtList)
                {

                    // Remove path from the file name. 
                    string fName = f.Substring(sourceDir.Length + 1);

                    try
                    {
                        // Will not overwrite if the destination file already exists.
                        File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, fName), true);
                    }

                    // Catch exception if the file was already copied. 
                    catch (IOException copyError)
                    {
                        MessageBox.Show("下載失敗!");
                        return;
                    }
                }
                MessageBox.Show("下載成功!");
            }

            catch (DirectoryNotFoundException dirNotFound)
            {
                MessageBox.Show("下載失敗!");
            }
        }
    }
}
