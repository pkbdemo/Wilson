﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wilson.Model
{
    public class WaterTemp
    {
        public decimal Cool_input { get; set; }
        public decimal Cool_output { get; set; }
        public decimal Cool_down_input { get; set; }
        public decimal Cool_down_output { get; set; }
        public string? Upd_time { get; set; }
        public int? Machine_num { get; set; }
    }
}
