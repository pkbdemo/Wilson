﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wilson.Model;
//using Wilson;

namespace Wilson
{
    public partial class MainPage : Form
    {
        Int32 count = 1;//拿來判斷當前號機的第幾個Sensor
        Int32 machine_num = 1;//拿來判斷當前取得的返回值是第幾號機
        string menu_type = "即時";
        WaterTemp temp = new WaterTemp();
        WaterTemp temp2 = new WaterTemp();
        SerialPort SerialPort = new SerialPort();
        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        System.Windows.Forms.Timer timerRefresh = new System.Windows.Forms.Timer();
        /// <summary>
        /// 把按-分割的字符串轉成byte[]
        /// </summary>
        /// <param name="s">string原數據</param>
        /// <returns></returns>
        public byte[] ToByte(string s)
        {
            return s.Split('-').AsParallel().Select(x => Convert.ToByte(x, 16)).ToArray();
        }
        public void GetPortContent()
        {
            //串口名稱
            SerialPort.PortName = config.AppSettings.Settings["portName"].Value;
            //波特率
            SerialPort.BaudRate = Convert.ToInt32(config.AppSettings.Settings["baudRate"].Value);
            //數據位
            SerialPort.DataBits = Convert.ToInt32(config.AppSettings.Settings["dataBits"].Value);
            //停止位
            SerialPort.StopBits = Enum.Parse<StopBits>(config.AppSettings.Settings["stopBits"].Value);
            //SerialPort.StopBits = StopBits.One;
            //校驗位
            SerialPort.Parity = Enum.Parse<Parity>(config.AppSettings.Settings["parity"].Value);
            //SerialPort.Parity = Parity.None;
            //打開串口
            SerialPort.Open();

            SerialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
        }
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                // Show all the incoming data in the port's buffer
                SerialPort sp = (SerialPort)sender;
                int length = sp.BytesToRead;
                byte[] buf = new byte[length];
                sp.Read(buf, 0, length);
             
                //    //接收七組16進制號碼 ex: 01 03 02 01 43 F8 25
                //    //01:ID
                //    //03:讀取
                //    //02:功能
                //    //01:溫度值-個位數乘256
                //    //43:溫度值-十位數乘16, 個位數就是原本數
                //    //計算出的三個溫度值進行加總 256+64+3=323 =>32.3度C
                //    //F8:CRC值
                //    //25:CRC值
                //step1 針對接收到的buff資料，先進行初步處理
                var num1 = Convert.ToInt32(buf[3].ToString()) * 256;
                var num2 = Convert.ToInt32(buf[4].ToString("00").Substring(0, 1)) * 16;
                var num3 = Convert.ToInt32(buf[4].ToString("00").Substring(0, 1));
                decimal resultNum = (num1 + num2 + num3) / 10;

                //判斷當前的ID感測器是幾號
                switch (buf[0].ToString())
                {
                    case "1":
                        if (label3.InvokeRequired)
                            temp.Cool_input = resultNum;
                        break;
                    case "2":
                        if (label4.InvokeRequired)
                            temp.Cool_output = resultNum;
                        break;
                    case "3":
                        if (label10.InvokeRequired)
                            temp.Cool_down_input = resultNum;
                        break;
                    case "4":
                        if (label12.InvokeRequired)
                            temp.Cool_down_output = resultNum;
                        break;
                }

                string cmd = "";
                byte[] hexValues;
                byte[] outBuffer;
                if (machine_num == 1)
                {
                    if (count == 5)//湊滿號機底下的四組感測溫度值，寫入DB
                    {
                        count = 1;
                        temp.Machine_num = 1;
                        Common comm = new Common();
                        comm.AddData(temp);
                      
                        temp = new WaterTemp();

                        if (label20.Text == "1號" && menu_type == "即時")
                        {//當前UI剛好也是一號機的話，順便刷新當前UI的溫度值
                            var read = comm.ReadData(1);
                            if (label3.InvokeRequired)
                                label3.Invoke(new Action<string>(n => { this.label3.Text = n.ToString(); }), read.Cool_input.ToString());
                            if (label4.InvokeRequired)
                                label4.Invoke(new Action<string>(n => { this.label4.Text = n.ToString(); }), read.Cool_output.ToString());
                            if (label10.InvokeRequired)
                                label10.Invoke(new Action<string>(n => { this.label10.Text = n.ToString(); }), read.Cool_down_input.ToString());
                            if (label12.InvokeRequired)
                                label12.Invoke(new Action<string>(n => { this.label12.Text = n.ToString(); }), read.Cool_down_output.ToString());
                            if (label6.InvokeRequired)
                                label6.Invoke(new Action<string>(n => { this.label6.Text = n.ToString(); }), (read.Cool_input - read.Cool_output).ToString());
                            if (label14.InvokeRequired)
                                label14.Invoke(new Action<string>(n => { this.label14.Text = n.ToString(); }), (read.Cool_down_input - read.Cool_down_output).ToString());
                        }
                        machine_num++;
                        cmd = config.AppSettings.Settings["cold_input2"].Value; //第二組裝置冷水入水設定
                        hexValues = ToByte(cmd);
                        outBuffer = hexValues;
                        SerialPort.Write(outBuffer, 0, outBuffer.Length);
                    }
                    else
                    {
                        count++;
                        if (count == 2)
                        {
                            cmd = config.AppSettings.Settings["cold_output1"].Value; //第一組裝置冷水出水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 3)
                        {
                            cmd = config.AppSettings.Settings["cool_down_input1"].Value; //第一組裝置冷卻水入水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 4)
                        {
                            cmd = config.AppSettings.Settings["cool_down_output1"].Value; //第一組裝置冷卻水出水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 5)
                        {
                            //判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
                            CheckOverTemp(machine_num, temp);
                        }
                    }
                }
                else if (machine_num == 2)
                {

                    if (count == 5)//湊滿號機底下的四組感測溫度值，寫入DB
                    {
                        count = 1;
                        temp.Machine_num = 2;
                        Common comm = new Common();
                        comm.AddData(temp);
                        //寫入DB後，判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
                       
                        temp = new WaterTemp();

                        if (label20.Text == "2號" && menu_type == "即時")//當前UI剛好也是二號機的話，順便刷新當前UI的溫度值
                        {
                            var read = comm.ReadData(2);
                            if (label3.InvokeRequired)
                                label3.Invoke(new Action<string>(n => { this.label3.Text = n.ToString(); }), read.Cool_input.ToString());
                            if (label4.InvokeRequired)
                                label4.Invoke(new Action<string>(n => { this.label4.Text = n.ToString(); }), read.Cool_output.ToString());
                            if (label10.InvokeRequired)
                                label10.Invoke(new Action<string>(n => { this.label10.Text = n.ToString(); }), read.Cool_down_input.ToString());
                            if (label12.InvokeRequired)
                                label12.Invoke(new Action<string>(n => { this.label12.Text = n.ToString(); }), read.Cool_down_output.ToString());
                            if (label6.InvokeRequired)
                                label6.Invoke(new Action<string>(n => { this.label6.Text = n.ToString(); }), (read.Cool_input - read.Cool_output).ToString());
                            if (label14.InvokeRequired)
                                label14.Invoke(new Action<string>(n => { this.label14.Text = n.ToString(); }), (read.Cool_down_input - read.Cool_down_output).ToString());
                        }
                        if (config.AppSettings.Settings["enable_third_sensor"].Value == "true")
                        {
                            machine_num++;
                            cmd = config.AppSettings.Settings["cold_input3"].Value; //第三組裝置冷水入水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        else
                            machine_num = 1;
                    }
                    else
                    {
                        count++;
                        if (count == 2)
                        {
                            cmd = config.AppSettings.Settings["cold_output2"].Value; //第二組裝置冷水出水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 3)
                        {
                            cmd = config.AppSettings.Settings["cool_down_input2"].Value; //第二組裝置冷卻水入水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 4)
                        {
                            cmd = config.AppSettings.Settings["cool_down_output2"].Value; //第二組裝置冷卻水出水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 5)
                        {
                            //判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
                            CheckOverTemp(machine_num, temp);
                        }
                    }
                }
                else if (machine_num == 3)
                {

                    if (count == 5)//湊滿號機底下的四組感測溫度值，寫入DB
                    {
                        count = 1;
                        temp.Machine_num = 3;
                        Common comm = new Common();
                        comm.AddData(temp);
                        //寫入DB後，判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
                        temp = new WaterTemp();
                        machine_num = 1;
                        if (label20.Text == "3號" && menu_type == "即時")//當前UI剛好也是三號機的話，順便刷新當前UI的溫度值
                        {
                            var read = comm.ReadData(3);
                            if (label3.InvokeRequired)
                                label3.Invoke(new Action<string>(n => { this.label3.Text = n.ToString(); }), read.Cool_input.ToString());
                            if (label4.InvokeRequired)
                                label4.Invoke(new Action<string>(n => { this.label4.Text = n.ToString(); }), read.Cool_output.ToString());
                            if (label10.InvokeRequired)
                                label10.Invoke(new Action<string>(n => { this.label10.Text = n.ToString(); }), read.Cool_down_input.ToString());
                            if (label12.InvokeRequired)
                                label12.Invoke(new Action<string>(n => { this.label12.Text = n.ToString(); }), read.Cool_down_output.ToString());
                            if (label6.InvokeRequired)
                                label6.Invoke(new Action<string>(n => { this.label6.Text = n.ToString(); }), (read.Cool_input - read.Cool_output).ToString());
                            if (label14.InvokeRequired)
                                label14.Invoke(new Action<string>(n => { this.label14.Text = n.ToString(); }), (read.Cool_down_input - read.Cool_down_output).ToString());
                        }
                    }
                    else
                    {
                        count++;
                        if (count == 2)
                        {
                            cmd = config.AppSettings.Settings["cold_output3"].Value; //第三組裝置冷水出水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 3)
                        {
                            cmd = config.AppSettings.Settings["cool_down_input3"].Value; //第三組裝置冷卻水入水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 4)
                        {
                            cmd = config.AppSettings.Settings["cool_down_output3"].Value; //第三組裝置冷卻水出水設定
                            hexValues = ToByte(cmd);
                            outBuffer = hexValues;
                            SerialPort.Write(outBuffer, 0, outBuffer.Length);
                        }
                        if (count == 5)
                        {
                            //判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
                            CheckOverTemp(machine_num, temp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }
        public MainPage()
        {
            InitializeComponent();
        }
        private void MainPage_Load(object sender, EventArgs e)
        {
            //計時器
            //timerRefresh.Interval = int.Parse(config.AppSettings.Settings["timer_second"].Value) * 1000;
            //timerRefresh.Tick += new EventHandler(timerRefresh_Tick);
            //timerRefresh.Start();

            Common comm2 = new Common();
            switch (label20.Text)
            {
                case "1號":
                    comm2 = new Common();
                    var read1 = comm2.ReadData(1);
                    label3.Text = read1.Cool_input.ToString();
                    label4.Text = read1.Cool_output.ToString();
                    label10.Text = read1.Cool_down_input.ToString();
                    label12.Text = read1.Cool_down_output.ToString();
                    label6.Text = (read1.Cool_input - read1.Cool_output).ToString();
                    label14.Text = (read1.Cool_down_input - read1.Cool_down_output).ToString();
                    break;
                case "2號":
                    comm2 = new Common();
                    var read2 = comm2.ReadData(2);
                    label3.Text = read2.Cool_input.ToString();
                    label4.Text = read2.Cool_output.ToString();
                    label10.Text = read2.Cool_down_input.ToString();
                    label12.Text = read2.Cool_down_output.ToString();
                    label6.Text = (read2.Cool_input - read2.Cool_output).ToString();
                    label14.Text = (read2.Cool_down_input - read2.Cool_down_output).ToString();
                    break;
                case "3號":
                    comm2 = new Common();
                    var read3 = comm2.ReadData(3);
                    label3.Text = read3.Cool_input.ToString();
                    label4.Text = read3.Cool_output.ToString();
                    label10.Text = read3.Cool_down_input.ToString();
                    label12.Text = read3.Cool_down_output.ToString();
                    label6.Text = (read3.Cool_input - read3.Cool_output).ToString();
                    label14.Text = (read3.Cool_down_input - read3.Cool_down_output).ToString();
                    break;
            }
            this.FormBorderStyle = FormBorderStyle.None;
            int x = (System.Windows.Forms.SystemInformation.WorkingArea.Width - this.Size.Width) / 2;
            int y = (System.Windows.Forms.SystemInformation.WorkingArea.Height - this.Size.Height) / 2;
            this.StartPosition = FormStartPosition.Manual; //窗體的位置由Location屬性決定
            this.Location = (Point)new Size(x, y);         //窗體的起始位置為(x,y)

            //預設即時文字變色
            //lbl_RealTime.ForeColor = Color.FromArgb(57, 64, 86);
            lbl_RealTime.ForeColor = Color.LightCoral;
            Thread th = new Thread(GetPortContent);
            th.IsBackground = true;
            th.Start();
        }

        private void lbl_Menu_Click(object sender, EventArgs e)
        {
            Label lbl_Menu = sender as Label;
            Common comm = new Common();
            WaterTemp read = new WaterTemp();
            switch (lbl_Menu.Text)
            {
                case "即時":
                    lbl_RealTime.ForeColor = Color.LightCoral;
                    lbl_LastAvg.ForeColor = Color.White;
                    lbl_LastWeekAvg.ForeColor = Color.White;
                    lbl_LastMonthAvg.ForeColor = Color.White;
                    lbl_RecordSearch.ForeColor = Color.White;
                    //這邊要開始根據切換的menu，顯示不同功能面的溫度值
                    menu_type = "即時";
                    comm = new Common();
                    read = new WaterTemp();
                    switch (label20.Text)
                    {
                        case "1號":
                            read = comm.ReadData(1);
                            break;
                        case "2號":
                            read = comm.ReadData(2);
                            break;
                        case "3號":
                            read = comm.ReadData(3);
                            break;
                    }
                    if (read != null)
                    {
                        label3.Text = Math.Round(read.Cool_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label4.Text = Math.Round(read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label10.Text = Math.Round(read.Cool_down_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label12.Text = Math.Round(read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label6.Text = Math.Round(read.Cool_input - read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label14.Text = Math.Round(read.Cool_down_input - read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                    }

                    break;
                case "昨天平均":
                    lbl_RealTime.ForeColor = Color.White;
                    lbl_LastAvg.ForeColor = Color.LightCoral;
                    lbl_LastWeekAvg.ForeColor = Color.White;
                    lbl_LastMonthAvg.ForeColor = Color.White;
                    lbl_RecordSearch.ForeColor = Color.White;
                    menu_type = "昨天平均";
                    comm = new Common();
                    read = new WaterTemp();
                    switch (label20.Text)
                    {
                        case "1號":
                            read = comm.ReadLastAvg(1);
                            break;
                        case "2號":
                            read = comm.ReadLastAvg(2);
                            break;
                        case "3號":
                            read = comm.ReadLastAvg(3);
                            break;
                    }
                    if (read != null)
                    {
                        label3.Text = Math.Round(read.Cool_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label4.Text = Math.Round(read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label10.Text = Math.Round(read.Cool_down_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label12.Text = Math.Round(read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label6.Text = Math.Round(read.Cool_input - read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label14.Text = Math.Round(read.Cool_down_input - read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                    }
                    break;
                case "上周平均":
                    lbl_RealTime.ForeColor = Color.White;
                    lbl_LastAvg.ForeColor = Color.White;
                    lbl_LastWeekAvg.ForeColor = Color.LightCoral;
                    lbl_LastMonthAvg.ForeColor = Color.White;
                    lbl_RecordSearch.ForeColor = Color.White;
                    menu_type = "上周平均";

                    comm = new Common();
                    read = new WaterTemp();
                    switch (label20.Text)
                    {
                        case "1號":
                            read = comm.ReadLastWeekAvg(1);
                            break;
                        case "2號":
                            read = comm.ReadLastWeekAvg(2);
                            break;
                        case "3號":
                            read = comm.ReadLastWeekAvg(3);
                            break;
                    }
                    if (read != null)
                    {
                        label3.Text = Math.Round(read.Cool_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label4.Text = Math.Round(read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label10.Text = Math.Round(read.Cool_down_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label12.Text = Math.Round(read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label6.Text = Math.Round(read.Cool_input - read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label14.Text = Math.Round(read.Cool_down_input - read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                    }
                    break;
                case "上月平均":
                    lbl_RealTime.ForeColor = Color.White;
                    lbl_LastAvg.ForeColor = Color.White;
                    lbl_LastWeekAvg.ForeColor = Color.White;
                    lbl_LastMonthAvg.ForeColor = Color.LightCoral;
                    lbl_RecordSearch.ForeColor = Color.White;
                    menu_type = "上月平均";
                    comm = new Common();
                    read = new WaterTemp();
                    switch (label20.Text)
                    {
                        case "1號":
                            read = comm.ReadLastMonthAvg(1);
                            break;
                        case "2號":
                            read = comm.ReadLastMonthAvg(2);
                            break;
                        case "3號":
                            read = comm.ReadLastMonthAvg(3);
                            break;
                    }
                    if (read != null)
                    {
                        label3.Text = Math.Round(read.Cool_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label4.Text = Math.Round(read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label10.Text = Math.Round(read.Cool_down_input, 1, MidpointRounding.AwayFromZero).ToString();
                        label12.Text = Math.Round(read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label6.Text = Math.Round(read.Cool_input - read.Cool_output, 1, MidpointRounding.AwayFromZero).ToString();
                        label14.Text = Math.Round(read.Cool_down_input - read.Cool_down_output, 1, MidpointRounding.AwayFromZero).ToString();
                    }
                    break;
                case "紀錄查詢":
                    lbl_RealTime.ForeColor = Color.White;
                    lbl_LastAvg.ForeColor = Color.White;
                    lbl_LastWeekAvg.ForeColor = Color.White;
                    lbl_LastMonthAvg.ForeColor = Color.White;
                    lbl_RecordSearch.ForeColor = Color.White;
                    menu_type = "紀錄查詢";
                    Record record = new Record();
                    record.Show();
                    break;

            }
        }
        private void read_menu_type_conten()
        {

        }
        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            try
            {
                //string cmd = "01-03-00-00-00-01-84-0A";//溫度值
                //string cmd = "09-05-00-00-FF-00-8D-72";//ID 1 繼電器開啟
                //string cmd = "09-05-00-00-00-00-cc-82"; //ID 1 繼電器關閉
                //string cmd = "09-02-00-00-00-08-78-84"; //狀態值
                temp = new WaterTemp();
                string cmd = "";
                byte[] hexValues;
                byte[] outBuffer;

                cmd = config.AppSettings.Settings["cold_input1"].Value; //第一組裝置冷水入水設定
                hexValues = ToByte(cmd);
                outBuffer = hexValues;
                SerialPort.Write(outBuffer, 0, outBuffer.Length);
            }
            catch (Exception ex) { var msg = ex.Message; }
        }

        private void btn_left(object sender, EventArgs e)
        {
            timerRefresh.Stop();
            if (config.AppSettings.Settings["enable_third_sensor"].Value == "true")
            {
                Common comm = new Common();
                switch (label20.Text)
                {
                    case "1號":
                        label20.Text = "3號";
                        comm = new Common();
                        var read3 = comm.ReadData(3);

                        label3.Text = read3.Cool_input.ToString();
                        label4.Text = read3.Cool_output.ToString();
                        label10.Text = read3.Cool_down_input.ToString();
                        label12.Text = read3.Cool_down_output.ToString();
                        label6.Text = (read3.Cool_input - read3.Cool_output).ToString();
                        label14.Text = (read3.Cool_down_input - read3.Cool_down_output).ToString();
                        break;
                    case "2號":
                        label20.Text = "1號";
                        comm = new Common();
                        var read1 = comm.ReadData(1);

                        label3.Text = read1.Cool_input.ToString();
                        label4.Text = read1.Cool_output.ToString();
                        label10.Text = read1.Cool_down_input.ToString();
                        label12.Text = read1.Cool_down_output.ToString();
                        label6.Text = (read1.Cool_input - read1.Cool_output).ToString();
                        label14.Text = (read1.Cool_down_input - read1.Cool_down_output).ToString();
                        break;
                    case "3號":
                        label20.Text = "2號";
                        comm = new Common();
                        var read2 = comm.ReadData(2);

                        label3.Text = read2.Cool_input.ToString();
                        label4.Text = read2.Cool_output.ToString();
                        label10.Text = read2.Cool_down_input.ToString();
                        label12.Text = read2.Cool_down_output.ToString();
                        label6.Text = (read2.Cool_input - read2.Cool_output).ToString();
                        label14.Text = (read2.Cool_down_input - read2.Cool_down_output).ToString();
                        break;
                }
            }
            else
            {
                Common comm = new Common();
                switch (label20.Text)
                {
                    case "1號":
                        label20.Text = "2號";
                        comm = new Common();
                        var read2 = comm.ReadData(2);

                        label3.Text = read2.Cool_input.ToString();
                        label4.Text = read2.Cool_output.ToString();
                        label10.Text = read2.Cool_down_input.ToString();
                        label12.Text = read2.Cool_down_output.ToString();
                        label6.Text = (read2.Cool_input - read2.Cool_output).ToString();
                        label14.Text = (read2.Cool_down_input - read2.Cool_down_output).ToString();
                        break;
                    case "2號":
                        label20.Text = "1號";
                        comm = new Common();
                        var read1 = comm.ReadData(1);

                        label3.Text = read1.Cool_input.ToString();
                        label4.Text = read1.Cool_output.ToString();
                        label10.Text = read1.Cool_down_input.ToString();
                        label12.Text = read1.Cool_down_output.ToString();
                        label6.Text = (read1.Cool_input - read1.Cool_output).ToString();
                        label14.Text = (read1.Cool_down_input - read1.Cool_down_output).ToString();
                        break;
                }
            }
            timerRefresh.Start();
        }

        private void btn_right(object sender, EventArgs e)
        {
            timerRefresh.Stop();
            if (config.AppSettings.Settings["enable_third_sensor"].Value == "true")
            {
                Common comm = new Common();
                switch (label20.Text)
                {
                    case "1號":
                        label20.Text = "2號";
                        comm = new Common();
                        var read2 = comm.ReadData(2);

                        label3.Text = read2.Cool_input.ToString();
                        label4.Text = read2.Cool_output.ToString();
                        label10.Text = read2.Cool_down_input.ToString();
                        label12.Text = read2.Cool_down_output.ToString();
                        label6.Text = (read2.Cool_input - read2.Cool_output).ToString();
                        label14.Text = (read2.Cool_down_input - read2.Cool_down_output).ToString();
                        break;
                    case "2號":
                        label20.Text = "3號";
                        comm = new Common();
                        var read3 = comm.ReadData(3);

                        label3.Text = read3.Cool_input.ToString();
                        label4.Text = read3.Cool_output.ToString();
                        label10.Text = read3.Cool_down_input.ToString();
                        label12.Text = read3.Cool_down_output.ToString();
                        label6.Text = (read3.Cool_input - read3.Cool_output).ToString();
                        label14.Text = (read3.Cool_down_input - read3.Cool_down_output).ToString();
                        break;
                    case "3號":
                        label20.Text = "1號";
                        comm = new Common();
                        var read1 = comm.ReadData(1);

                        label3.Text = read1.Cool_input.ToString();
                        label4.Text = read1.Cool_output.ToString();
                        label10.Text = read1.Cool_down_input.ToString();
                        label12.Text = read1.Cool_down_output.ToString();
                        label6.Text = (read1.Cool_input - read1.Cool_output).ToString();
                        label14.Text = (read1.Cool_down_input - read1.Cool_down_output).ToString();
                        break;
                }
            }
            else
            {
                Common comm = new Common();
                switch (label20.Text)
                {
                    case "1號":
                        label20.Text = "2號";
                        comm = new Common();
                        var read2 = comm.ReadData(2);

                        label3.Text = read2.Cool_input.ToString();
                        label4.Text = read2.Cool_output.ToString();
                        label10.Text = read2.Cool_down_input.ToString();
                        label12.Text = read2.Cool_down_output.ToString();
                        label6.Text = (read2.Cool_input - read2.Cool_output).ToString();
                        label14.Text = (read2.Cool_down_input - read2.Cool_down_output).ToString();
                        break;
                    case "2號":
                        label20.Text = "1號";
                        comm = new Common();
                        var read1 = comm.ReadData(1);

                        label3.Text = read1.Cool_input.ToString();
                        label4.Text = read1.Cool_output.ToString();
                        label10.Text = read1.Cool_down_input.ToString();
                        label12.Text = read1.Cool_down_output.ToString();
                        label6.Text = (read1.Cool_input - read1.Cool_output).ToString();
                        label14.Text = (read1.Cool_down_input - read1.Cool_down_output).ToString();
                        break;
                }
            }
            timerRefresh.Start();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            byte[] hexValues;
            byte[] outBuffer;
            var cmd = config.AppSettings.Settings["cold_input1"].Value; //第一組裝置冷水入水設定
            hexValues = ToByte(cmd);
            outBuffer = hexValues;
            SerialPort.Write(outBuffer, 0, outBuffer.Length);
        }

        /// <summary>
        /// //寫入DB後，判斷是否冷卻水超過設定溫度，超過的話，就啟動繼電器
        /// </summary>
        /// <param name="machine_num"></param>
        /// <param name="temp"></param>
        /// <returns></returns>
        private void CheckOverTemp(int machine_num, WaterTemp temp)
        {
            decimal threshhold_temp = 0;
            if (machine_num == 1)
                threshhold_temp = decimal.Parse(config.AppSettings.Settings["threshhold_temp_setting1"].Value); //第一組冷卻水入水觸發繼電器溫度值
            if (machine_num == 2)
                threshhold_temp = decimal.Parse(config.AppSettings.Settings["threshhold_temp_setting2"].Value); //第二組冷卻水入水觸發繼電器溫度值
            if (machine_num == 3)
                threshhold_temp = decimal.Parse(config.AppSettings.Settings["threshhold_temp_setting3"].Value); //第三組冷卻水入水觸發繼電器溫度值
            if (temp.Cool_down_input > threshhold_temp)
                StartGate(machine_num);
            else
                CloseGate(machine_num);
        }

        /// <summary>
        /// 啟動繼電器
        /// </summary>
        /// <param name="machine_num"></param>
        private void StartGate(int machine_num)
        {
            byte[] hexValues;
            byte[] outBuffer;
            string cmd = "";
            if (machine_num == 1)
                cmd = config.AppSettings.Settings["gate1_open"].Value; //第一組裝置繼電器開啟
            if (machine_num == 2)
                cmd = config.AppSettings.Settings["gate2_open"].Value; //第二組裝置繼電器開啟
            if (machine_num == 3)
                cmd = config.AppSettings.Settings["gate3_open"].Value; //第三組裝置繼電器開啟

            hexValues = ToByte(cmd);
            outBuffer = hexValues;
            SerialPort.Write(outBuffer, 0, outBuffer.Length);
        }
        /// <summary>
        /// 關閉繼電器
        /// </summary>
        /// <param name="machine_num"></param>
        private void CloseGate(int machine_num)
        {
            byte[] hexValues;
            byte[] outBuffer;
            string cmd = "";
            if (machine_num == 1)
                cmd = config.AppSettings.Settings["gate1_close"].Value; //第一組裝置繼電器關閉
            if (machine_num == 2)
                cmd = config.AppSettings.Settings["gate2_close"].Value; //第二組裝置繼電器關閉
            if (machine_num == 3)
                cmd = config.AppSettings.Settings["gate3_close"].Value; //第三組裝置繼電器關閉

            hexValues = ToByte(cmd);
            outBuffer = hexValues;
            SerialPort.Write(outBuffer, 0, outBuffer.Length);
        }
    }
}
