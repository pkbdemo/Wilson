﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wilson.Model;
//using Wilson;

namespace Wilson
{
    public partial class Record : Form
    {
        public static int global_RecordMachineNum = 0;
        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        public Record()
        {
            InitializeComponent();
        }
        private void Record_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            int x = (System.Windows.Forms.SystemInformation.WorkingArea.Width - this.Size.Width) / 2;
            int y = (System.Windows.Forms.SystemInformation.WorkingArea.Height - this.Size.Height) / 2;
            this.StartPosition = FormStartPosition.Manual; //窗體的位置由Location屬性決定
            this.Location = (Point)new Size(x, y);         //窗體的起始位置為(x,y)
        }

        /// <summary>
        /// 返回按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label22_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// 日匯總表按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label5_Click(object sender, EventArgs e)
        {
            if (label2.Text == "1號")
                global_RecordMachineNum = 1;
            if (label2.Text == "2號")
                global_RecordMachineNum = 2;
            if (label2.Text == "3號")
                global_RecordMachineNum = 3;
            DailyReport dailyReport = new DailyReport();
            dailyReport.Show();
        }

        /// <summary>
        /// 時匯總表按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label6_Click(object sender, EventArgs e)
        {
            if (label2.Text == "1號")
                global_RecordMachineNum = 1;
            if (label2.Text == "2號")
                global_RecordMachineNum = 2;
            if (label2.Text == "3號")
                global_RecordMachineNum = 3;
            HourlyReport hourlyReport = new HourlyReport();
            hourlyReport.Show();
        }

        /// <summary>
        /// 月匯總表按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label7_Click(object sender, EventArgs e)
        {
            if (label2.Text == "1號")
                global_RecordMachineNum = 1;
            if (label2.Text == "2號")
                global_RecordMachineNum = 2;
            if (label2.Text == "3號")
                global_RecordMachineNum = 3;
            MonthlyReport monthlyReport = new MonthlyReport();
            monthlyReport.Show();
        }

        /// <summary>
        /// 方向左按鍵
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_left(object sender, EventArgs e)
        {
            
            if (config.AppSettings.Settings["enable_third_sensor"].Value == "true")
            {
                switch (label2.Text)
                {
                    case "1號":
                        label2.Text = "3號";
                        break;
                    case "2號":
                        label2.Text = "1號";
                        break;
                    case "3號":
                        label2.Text = "2號";
                        break;
                }
            }
            else
            {
                switch (label2.Text)
                {
                    case "1號":
                        label2.Text = "2號";
                        break;
                    case "2號":
                        label2.Text = "1號";
                        break;
                }
            }
          
        }

        /// <summary>
        /// 方向右按鍵
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_right(object sender, EventArgs e)
        {
            if (config.AppSettings.Settings["enable_third_sensor"].Value == "true")
            {
                switch (label2.Text)
                {
                    case "1號":
                        label2.Text = "2號";
                        break;
                    case "2號":
                        label2.Text = "3號";
                        break;
                    case "3號":
                        label2.Text = "1號";
                        break;
                }
            }
            else
            {
                switch (label2.Text)
                {
                    case "1號":
                        label2.Text = "2號";
                        break;
                    case "2號":
                        label2.Text = "1號";
                        break;
                }
            }
        }
    }
}
