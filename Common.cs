﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO.Ports;
using Wilson.Model;
using Newtonsoft.Json;

namespace Wilson
{
    internal class Common
    {
        /// <summary>建立資料庫連線</summary>
        /// <param name="database">資料庫名稱</param>
        /// <returns></returns>
        public SQLiteConnection OpenConnection(string database)
        {
            var conntion = new SQLiteConnection()
            {
                ConnectionString = $"Data Source={database};Version=3;New=False;Compress=True;"
            };
            if (conntion.State == ConnectionState.Open) conntion.Close();
            conntion.Open();
            return conntion;
        }

        /// <summary>新增\修改\刪除資料</summary>
        /// <param name="database">資料庫名稱</param>
        /// <param name="sqlManipulate">資料操作的 SQL 語句</param>
        public void Manipulate(string sqlManipulate)
        {
            var connection = OpenConnection("RadiantDB.db");
            var command = new SQLiteCommand(sqlManipulate, connection);
            var mySqlTransaction = connection.BeginTransaction();
            try
            {
                command.Transaction = mySqlTransaction;
                command.ExecuteNonQuery();
                mySqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                mySqlTransaction.Rollback();
                throw (ex);
            }
            if (connection.State == ConnectionState.Open) connection.Close();
        }

        /// <summary>讀取資料</summary>
        /// <param name="database">資料庫名稱</param>
        /// <param name="sqlQuery">資料查詢的 SQL 語句</param>
        /// <returns></returns>
        public DataTable GetDataTable(string sqlQuery)
        {
            var connection = OpenConnection("RadiantDB.db");
            var dataAdapter = new SQLiteDataAdapter(sqlQuery, connection);
            var myDataTable = new DataTable();
            var myDataSet = new DataSet();
            myDataSet.Clear();
            dataAdapter.Fill(myDataSet);
            myDataTable = myDataSet.Tables[0];
            if (connection.State == ConnectionState.Open) connection.Close();
            return myDataTable;
        }


        public WaterTemp ReadData(int machine_num)
        {
            string cmd = @" SELECT * from water_temp  WHERE machine_num =" + machine_num.ToString() + " order by upd_time DESC LIMIT 1 ";
            var resultDataTable = GetDataTable(cmd);
            if (resultDataTable.Rows.Count > 0)
            {
                //dataTable=>Json=>ModelClass
                string serialized = JsonConvert.SerializeObject(resultDataTable, Formatting.Indented);
                List<WaterTemp> result = JsonConvert.DeserializeObject<List<WaterTemp>>(serialized, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                return result.FirstOrDefault();
            }
            else
                return null;
        }
        public WaterTemp ReadLastAvg(int machine_num)
        {
            WaterTemp result = new WaterTemp();
            DateTime today = DateTime.Today;
            DateTime previousDay = today.AddDays(-1);
            string cmd = @" SELECT * FROM water_temp WHERE upd_time BETWEEN datetime('" + previousDay.ToString("u") + "') AND datetime('" + today.ToString("u") + "') and machine_num =" + machine_num.ToString() ;
            var resultDataTable = GetDataTable(cmd);
            if (resultDataTable.Rows.Count > 0)
            {
                string serialized = JsonConvert.SerializeObject(resultDataTable, Formatting.Indented);
                List<WaterTemp> result_list = JsonConvert.DeserializeObject<List<WaterTemp>>(serialized, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                
                result.Cool_input = result_list.Select(x => x.Cool_input).Average();
                result.Cool_output = result_list.Select(x => x.Cool_output).Average();
                result.Cool_down_input = result_list.Select(x => x.Cool_down_input).Average();
                result.Cool_down_output = result_list.Select(x => x.Cool_down_output).Average();
                return result;
            }
            else
                return null;
        }
        public WaterTemp ReadLastWeekAvg(int machine_num)
        {
            WaterTemp result = new WaterTemp();
            DayOfWeek weekStart = DayOfWeek.Monday;
            DateTime startingDate = DateTime.Today;
            while (startingDate.DayOfWeek != weekStart)
                startingDate = startingDate.AddDays(-1);
            DateTime previousWeekStart = startingDate.AddDays(-7);
            DateTime previousWeekEnd = startingDate.AddDays(-1);
            string cmd = @" SELECT * FROM water_temp WHERE upd_time BETWEEN datetime('" + previousWeekStart.ToString("u") + "') AND datetime('" + previousWeekEnd.ToString("u") + "') and machine_num =" + machine_num.ToString();
            var resultDataTable = GetDataTable(cmd);
            if (resultDataTable.Rows.Count > 0)
            {
                string serialized = JsonConvert.SerializeObject(resultDataTable, Formatting.Indented);
                List<WaterTemp> result_list = JsonConvert.DeserializeObject<List<WaterTemp>>(serialized, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                result.Cool_input = result_list.Select(x => x.Cool_input).Average();
                result.Cool_output = result_list.Select(x => x.Cool_output).Average();
                result.Cool_down_input = result_list.Select(x => x.Cool_down_input).Average();
                result.Cool_down_output = result_list.Select(x => x.Cool_down_output).Average();
                return result;
            }
            else
                return null;
        }
        public WaterTemp ReadLastMonthAvg(int machine_num)
        {
            WaterTemp result = new WaterTemp();
            DateTime previousDate = DateTime.Now.AddMonths(-1);
            var startDate = new DateTime(previousDate.Year, previousDate.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            string cmd = @" SELECT * FROM water_temp WHERE upd_time BETWEEN datetime('" + startDate.ToString("u") + "') AND datetime('" + endDate.ToString("u") + "') and machine_num =" + machine_num.ToString();
            var resultDataTable = GetDataTable(cmd);
            if (resultDataTable.Rows.Count > 0)
            {
                string serialized = JsonConvert.SerializeObject(resultDataTable, Formatting.Indented);
                List<WaterTemp> result_list = JsonConvert.DeserializeObject<List<WaterTemp>>(serialized, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                result.Cool_input = result_list.Select(x => x.Cool_input).Average();
                result.Cool_output = result_list.Select(x => x.Cool_output).Average();
                result.Cool_down_input = result_list.Select(x => x.Cool_down_input).Average();
                result.Cool_down_output = result_list.Select(x => x.Cool_down_output).Average();
                return result;
            }
            else
                return null;
        }

        public void AddData(WaterTemp param) {

        string cmd = @"
            INSERT INTO water_temp (cool_input,cool_output ,cool_down_input, cool_down_output, upd_time, machine_num)
            VALUES  (" + param.Cool_input.ToString() + ", " + param.Cool_output.ToString() + ", " + param.Cool_down_input.ToString() + ", " + param.Cool_down_output.ToString() + ", '" + DateTime.Now.ToString("u") + "', " + param.Machine_num + ")";
        Manipulate(cmd);
          
        }

        public DataTable QueryDailayReport(int machine_num)
        {
            string cmd = "SELECT date(upd_time) 時間, ROUND(AVG(cool_input),1)冰水入水, ROUND(AVG(cool_output),1) 冰水出水,  ROUND(AVG(cool_input) - AVG(cool_output),1) 冰水溫差 , ROUND(AVG(cool_down_input),1) 冷卻水入水,  ROUND(AVG(cool_down_output),1)冷卻水出水,  ROUND(AVG(cool_down_input) - AVG(cool_down_output),1) 冷卻水溫差 FROM water_temp where machine_num = " + machine_num + " GROUP BY  date(upd_time)";
           
            var resultDataTable = GetDataTable(cmd);
            return resultDataTable;
        }
        public DataTable QueryMonthlyReport(int machine_num)
        {
            string cmd = "SELECT strftime('%Y-%m', upd_time) 時間, ROUND( AVG(cool_input),1) 冰水入水,ROUND(AVG(cool_output),1)冰水出水,  ROUND(AVG(cool_input) - AVG(cool_output),1) 冰水溫差 , ROUND(AVG(cool_down_input),1)冷卻水入水,  ROUND(AVG(cool_down_output),1) 冷卻水出水, ROUND( AVG(cool_down_input) - AVG(cool_down_output),1)冷卻水溫差 FROM water_temp where machine_num =" + machine_num + " GROUP BY  strftime('%Y-%m', upd_time)";

            var resultDataTable = GetDataTable(cmd);
            return resultDataTable;
        }
        public DataTable QueryHourlyReport(int machine_num)
        {
            string cmd = "SELECT strftime('%Y-%m-%d %H', upd_time) 時間, ROUND( AVG(cool_input),1) 冰水入水,ROUND(AVG(cool_output),1)冰水出水,  ROUND(AVG(cool_input) - AVG(cool_output),1) 冰水溫差 , ROUND(AVG(cool_down_input),1)冷卻水入水,  ROUND(AVG(cool_down_output),1) 冷卻水出水, ROUND( AVG(cool_down_input) - AVG(cool_down_output),1)冷卻水溫差 FROM water_temp where machine_num =" + machine_num + " GROUP BY  strftime('%Y-%m-%d %H', upd_time)";

            var resultDataTable = GetDataTable(cmd);
            return resultDataTable;
        }


    }
}
